---
title: Rise
link: http://rise.rodrigo3d.com
#image: http://rise.rodrigo3d.com/screenshot.png
description: Email do google, google admin
---

A template of README best practices to make your README simple to understand and easy to use. 

## Table of Contents

- [Instalação Local](#instalação-local)
- [Login](#login)
- [Criando e editando um APP](#criando-e-editando-um-app)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)

## Instalação

Clone o projeto.

```sh
git clone git@bitbucket.org:rodrigo3d/rise.git
```
Após clonar o projeto crie um banco de dados Mysql, agora basta preencher as informações no assistente de instalação.

## Implantação no Heroku



## Login

Efetuando o login no Heroku

```sh
# Efeuando o login
heroku login

Após efe

λ heroku.cmd login
heroku: Enter your login credentials
Email [me@rodrigo3d.com]: me@rodrigo3d.com
Password: ******************
Logged in as me@rodrigo3d.com
```

## Criando e editando um APP

Após o login podemos criar uma applicação, banco de dados, definir variaveis de ambiente e etc...


1. Criando e exluindo uma aplicação no Heroku.
   * Criar `heroku apps:create <nome da aplicação>`
   * Excluir: `heroku apps:delete <nome da aplicação>`

## Deploy do Wordpress

```sh
heroku create <nome da aplicação>
```

1. Criando o APP no Heroku e renomeando

```sh
heroku apps:create heroku-teste



```

## banco de dados

```sh
#MariaDB
$ heroku addons:create jawsdb-maria:kitefin

```

* C1.riando bando de dados

## laravel-realworld-example-app

```sh
git clone 

cd 
```

## Usage

Replace the contents of `README.md` with your project's:

- Name
- Description
- Installation instructions
- Usage instructions
- Support instructions
- Contributing instructions

Feel free to remove any sections that aren't applicable to your project.

## Support

Please [open an issue](https://github.com/fraction/readme-boilerplate/issues/new) for support.

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/). Create a branch, add commits, and [open a pull request](https://github.com/fraction/readme-boilerplate/compare/).
