---
title: Advogados
date: 2018-02-23 07:00:00
published: false
categories:
- source
- sistemas
- projetos
tags:
- php
layout: post
comments: true
description: Sistema para Hospital
---

A configuração de ambiente possui três valores, `development`, `testing` e `production` podendo ser alterada no arquivo `index.php`, conforme exemplo abaixo:

```php
define('ENVIRONMENT', 'testing');
```

A configuração do banco de dados encontra-se no arquivo `application/config/database.php` e precisamos efetuar as alterações conforme exemplo abaixo:

```php
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'hms';

$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
```

### Info
```yml
tecnologia: PHP, MYSQL
deploy: https://rodrigo3d.herokuapp.com/hms
repository: https://gitlab.com/rodrigo3d/hms
```

