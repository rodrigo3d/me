---
title: Links
permalink: "/links/"
published: false
layout: page
description: Descrição Sobre Mim
---

<ol>
  {% for item in site.links %}
  <li>
    <a href="{{ item.link }}">{{ item.name }}</a>
    — {{ item.description }}
  </li>
  {% endfor %}
</ol>
