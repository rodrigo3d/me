"use strict";
/**
 * demo-data module
 * @module demo-data
 * @see module:index
 */

const gulp = require("gulp");
const shell = require("gulp-shell");
const imagemin = require("gulp-imagemin");
const mergeJSON = require("gulp-merge-json");

gulp.task("svg-icon", function() {
  /**
   * Tarefa do Gulp para minimizar arquivos de Imagem
   * src: ./src/img
   * dest: ./source/assets/img, ./_site/assets/img
   *
   */
  gulp
    .src("./src/svg-icon/0*.json")
    .pipe(
      mergeJSON({
        fileName: "svg-icon.json",
        edit: (parsedJson, file) => {
          if (parsedJson.someValue) {
            delete parsedJson.otherValue;
          }
          return parsedJson;
        }
      })
    )
    .pipe(gulp.dest("./src/svg-icon"))
    .pipe(
      shell(
        [
          "node node_modules/svg-icon/bin/cli.js build --source <%= file.path %> --target ./src/svg-icon --name icon"
        ],
        { verbose: true }
      )
    );
  /**
   * Tarefa do Gulp para minimizar o arquivo de Imagem
   * src: ./src/svg-icon/svg-icon.json
   * dest: ./source/assets/img/svg-symbols.svg | ./_site/assets/img/svg-symbols.svg
   */
  gulp
    .src("./src/svg-icon/icon/svg-symbols.svg")
    .pipe(
      imagemin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 5,
        svgoPlugins: [{ removeViewBox: false }]
      })
    )
    .pipe(gulp.dest("./source/assets/img"))
    .pipe(gulp.dest("./_site/assets/img"));
});
