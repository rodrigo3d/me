"use strict";
/**
 * demo-data module
 * @module demo-data
 * @see module:index
 */

const gulp = require("gulp");
const shell = require("gulp-shell");
const imagemin = require("gulp-imagemin");
const mergeJSON = require("gulp-merge-json");
var plumber = require("gulp-plumber");
var sass = require("gulp-sass");

/*
 * Tarefa do Gulp para compilar arquivos SASS.
 * src: ./src/scss/custom.scss
 * dest: ./source/assets/css, ./_site/assets/css
 */
gulp.task("sass", function() {
  // browserSync.notify(messages.sass);
  return gulp
    .src([
      "./src/scss/main.scss",
      "./src/scss/minima.scss",
      "./src/scss/svg.scss",
      "./src/scss/flexbox.scss"
    ])
    .pipe(plumber())
    .pipe(sass.sync({ outputStyle: /*"expanded"*/ "compressed" }).on("error", sass.logError))
    // .pipe(cleanCSS())
    .pipe(rename({ suffix: ".min" }))
    // .pipe(header(banner, { pkg: pkg }))
    .pipe(gulp.dest("./source/assets/css"))
    .pipe(gulp.dest("./_site/assets/css"))
    .pipe(browserSync.stream())
    .pipe(plumber.stop());
});
