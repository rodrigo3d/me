---
title: Manutenção Febract
link: https://febract-org-rodrigo3d.netlify.com
image: https://febract-org-rodrigo3d.netlify.com/screenshot.png
tags:
- php
categories:
- php
repository: http://github.com/rodrigo3d
deploy: Heroku
status: online
published: true
---

Este projeto tem como objetivo manter a publicação do domínio: `febract.org`
Para que o projeto funcione corretamente é necessário seguir o conteúdo deste README.

## Requisitos
Para começar, você precisará do seguinte:
 - Um servidor web com escrita URL
 - PHP >= 5.6 

## Instalação
Execute o comando abaixo a partir do diretório no qual deseja instalar este projeto.
```
git clone https://github.com/rodrigo3d/febract.org.git
```
## Link
Para visualizar o projeto em produção acesse: [http://febract.org](http://febract.org)

## Licença
Este projeto está licenciado sob a licença MIT - veja o arquivo [LICENSE](LICENSE) para detalhes.
