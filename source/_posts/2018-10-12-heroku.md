---
title: Heroku
date: 2018-10-12 00:00:02-0700
published: true
featured: true
categories:
- deploy
tags:
- heroku
- php
image: "/assets/img/"
description: Meu website
---

A template of README best practices to make your README simple to understand and easy to use. 

## Table of Contents
- [Instalação laravel-realworld-example-app](#laravel-realworld-example-app)
- [Instalação](#instalação)
- [Login](#login)
- [Criando e editando um APP](#criando-e-editando-um-app)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)

## Instalação

Instalando o Heroku CLI, para mais informações consulte a [documentação](https://devcenter.heroku.com/articles/heroku-cli) do Heroku CLI.

```sh
npm install -g heroku 
```

## Login

Efetuando o login no Heroku

```sh
# Efeuando o login
heroku login

Após efe

λ heroku.cmd login
heroku: Enter your login credentials
Email [me@rodrigo3d.com]: me@rodrigo3d.com
Password: ******************
Logged in as me@rodrigo3d.com
```

## Criando e editando um APP

Após o login podemos criar uma applicação, banco de dados, definir variaveis de ambiente e etc...


1. Criando e exluindo uma aplicação no Heroku.
   * Criar `heroku apps:create <nome da aplicação>`
   * Excluir: `heroku apps:delete <nome da aplicação>`

## Deploy do Wordpress

```sh
heroku create <nome da aplicação>
```

1. Criando o APP no Heroku e renomeando

```sh
heroku apps:create heroku-teste



```

## banco de dados

```sh
#MariaDB
$ heroku addons:create jawsdb-maria:kitefin

```

* C1.riando bando de dados

## laravel-realworld-example-app

```sh
git clone 

cd 
```

## Usage

Replace the contents of `README.md` with your project's:

- Name
- Description
- Installation instructions
- Usage instructions
- Support instructions
- Contributing instructions

Feel free to remove any sections that aren't applicable to your project.

## Support

Please [open an issue](https://github.com/fraction/readme-boilerplate/issues/new) for support.

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/). Create a branch, add commits, and [open a pull request](https://github.com/fraction/readme-boilerplate/compare/).
