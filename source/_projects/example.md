---
title: Example
link: https://rodrigo3d.com
image: https://rodrigo3d.com/screenshot.png
tags:
- 1
- 2
categories:
- 1
- 2
repository: http://github.com/rodrigo3d
date: '2018-10-18 00:30:00'
deploy: Heroku
status: online
published: true
---

# O que é o Lorem Ipsum?

O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. 

> O Lorem Ipsum tem vindo a ser o texto padrão usado por estas indústrias desde o ano de 1500, quando uma misturou os caracteres de um texto para criar um espécime de livro. 

## Porque é que o usamos?

É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando analisa a sua mancha gráfica. Logo, o uso de Lorem Ipsum leva a uma distribuição mais ou menos normal de letras, ao contrário do uso de "Conteúdo aqui, conteúdo aqui", tornando-o texto legível. Muitas ferramentas de publicação electrónica e editores de páginas web usam actualmente o Lorem Ipsum como o modelo de texto usado por omissão, e uma pesquisa por "lorem ipsum" irá encontrar muitos websites ainda na sua infância. Várias versões têm evoluído ao longo dos anos, por vezes por acidente, por vezes propositadamente (como no caso do humor).
