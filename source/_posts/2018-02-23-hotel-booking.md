---
title: Hotel Booking
date: 2018-02-23 07:00:00 Z
published: false
categories:
- source
- sistemas
tags:
- php
layout: post
comments: true
description: Sistema de Reservas em PHP
---

No **TekCast** de hoje apresentamos o [PHP Hotel Booking](http://www.netartmedia.net/php-hotel-booking), uma aplicação completa para criação de sites de reservas de quartos, salas e até veículos. O sistema pode ser personalizado para qualquer tipo de reserva.

No vídeo mostramos como realizar a instalação, configuração básica e uso das principais funcionalidades, tudo de forma simples e fácil para que qualquer pessoa possa instalar e rodar seu script PHP de reservas.

Se você esta em busca de uma solução para reservas, o PHP Hotel Booking é uma ótima alternativa, principalmente por ser leve e totalmente Open Source.

## PHP Hotel Booking - Rerserva de Salas Online

![Screenshot](https://tekzoom.com.br/content/images/2018/02/PHP-Hotel-Booking.jpg)

Este é um daqueles scripts PHP que num primeiro momento parece sem muita utilidade, mas depois de testa-lo e verificar todas as suas possibilidades acabamos mudando de ideia.

O **PHP Hotel Booking** foi desenvolvido para ser prático, leve e totalmente customizável. Seu conteúdo pode ser modificado e todos seus textos podem ser localizados, fazendo com que o sistema possa ser utilizado em praticamente qualquer tipo de projeto.

Depois de acompanhar o nosso vídeo, será muito simples publicar seu próprio portal de reservas.

## Baixe grátis o PHP Hotel Booking

![Screenshot](https://tekzoom.com.br/content/images/2018/02/PHP-Hotel-booking-translate.jpg)

Como já falado aqui, a tradução é muito fácil e editando apenas um arquivo é possível deixar toda a aplicação em português. Ah! Na verdade, você pode fazer a tradução para qualquer idioma.

Uma dica é navegar em todas as páginas do sistema e ir anotando o texto que seria interessante ser utilizado naquele local. Desta forma tudo ficará muito mais simples.

Para baixar o PHP Hotel Booking, é só [clicar aqui](http://www.netartmedia.net/php-hotel-booking). Não esqueça também de conferir a documentação oficial para entender melhor como tudo funciona.

## Referencias

* [Tekzoom](https://tekzoom.com.br/crie-um-sistema-php-para-reservas-de-hoteis-carros-e-salas/)
* [NetArt Media](http://www.netartmedia.net/php-hotel-booking)
