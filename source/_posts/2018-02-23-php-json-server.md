---
title: PHP Json Server
date: 2018-02-23 00:00:01 Z
published: false
categories:
- source
- I love Jekyll
tags:
- jekyll
- template
layout: post
description: First steps to use this template
---

…or create a new repository on the command line
echo "# api.rodrigo3d.com" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin git@github.com:rodrigo3d/api.rodrigo3d.com.git
git push -u origin master
…or push an existing repository from the command line
git remote add origin git@github.com:rodrigo3d/api.rodrigo3d.com.git
git push -u origin master
…or import code from another repository
You can initialize this repository with code from a Subversion, Mercurial, or TFS project.
