"use strict";

var cp = require("child_process");

/*
 * Dependências para desenvolvimento.
 */
var prefixer = require('autoprefixer-stylus');
var browserSync = require("browser-sync");
var del = require("del");
var gulp = require("gulp");
var cleanCSS = require("gulp-clean-css");
var concat = require("gulp-concat");
var header = require("gulp-header");
var imagemin = require("gulp-imagemin");
var plumber = require("gulp-plumber");
var rename = require("gulp-rename");
var stylus = require('gulp-stylus');
var sass = require("gulp-sass");
var uglify = require("gulp-uglify");
var runSequence = require("run-sequence");
var jeet = require('jeet');
var koutoSwiss = require('kouto-swiss');
var rupture = require('rupture');

var pkg = require("./package.json");

require('./gulp/svg-icon.js');
require('./gulp/real-favicon.js');

/*
 * Conteúdo do banner
 */
var banner = [
  "/*!\n",
  " * " + pkg.name.toUpperCase(),
  " - v<%= pkg.version %> (<%= pkg.homepage %>)\n",
  " * Copyright " + new Date().getFullYear(),
  " <%= pkg.author %> (https://rodrigo3d.com)\n",
  " * Licenciado sob <%= pkg.license %> (https://github.com/rodrigo3d/<%= pkg.name %>/blob/master/LICENSE)\n",
  " */\n",
  ""
].join("");

/*
 * Mensagens
 */
var messages = {
  js:
    '<span style="color: red">Running:</span> $ Compilando arquivos JavaScript',
  sass: '<span style="color: red">Running:</span> $ Compilando arquivos SASS',
  stylus:
    '<span style="color: red">Running:</span> $ Compilando arquivos Stylus',
  jekyllBuild: '<span style="color: red">Running:</span> $ Jekyll Build'
};

/*
 * Tarefa do Gulp, identifica o Sistema Operacional, compila e gera o site em Jekyll
 */
var jekyllCommand = /^win/.test(process.platform) ? "windows" : "linux";

gulp.task("jekyll-build", function(done) {
  browserSync.notify(messages.jekyllBuild);
  if (jekyllCommand === "windows") {
    return cp
      .spawn("jekyll.bat", ["build"], { stdio: "inherit" })
      .on("close", done);
  } else if (jekyllCommand === "linux") {
    return cp
      .spawn("bundle", ["exec", "jekyll build"], { stdio: "inherit" })
      .on("close", done);
  }
});

/*
 * Tarefa do Gulp, recompila e recarrega a página
 */
gulp.task("jekyll-rebuild", ["jekyll-build"], function() {
  browserSync.reload();
});

/*
 * Tarefa do Gulp, configura o browserSync
 * Aguarde o jekyll-build em seguida inicia o servidor
 */
gulp.task("browser-sync", ["jekyll-build"], function() {
  browserSync.init({
    port: 3002,
    ui: { port: 3003 },
    server: { baseDir: "./_site", directory: false, index: "index.html" }
  });
});

/*
 * Tarefa do Gulp para compilar arquivos .styl e concatena-los em um único arquivo.
 * src: ./src/styl
 * dest: ./source/assets/css
 */
gulp.task('stylus', function () {
  browserSync.notify(messages.stylus)
  gulp.src('./src/styl/stylus.styl')
    .pipe(plumber())
    .pipe(stylus({
      use: [koutoSwiss(), prefixer(), jeet(), rupture()],
      compress: false
    }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(header(banner, { pkg: pkg }))
    .pipe(gulp.dest('./source/assets/css'))
    .pipe(gulp.dest('./_site/assets/css'))
    .pipe(browserSync.stream())
    .pipe(plumber.stop());
});

/*
 * Tarefa do Gulp para compilar arquivos SASS.
 * src: ./src/scss/custom.scss
 * dest: ./source/assets/css, ./_site/assets/css
 */
gulp.task("sass", function() {
  browserSync.notify(messages.sass);
  return gulp
    .src([
      "./src/scss/main.scss",
      "./src/scss/minima.scss",
      "./src/scss/svg.scss",
      "./src/scss/flexbox.scss"
    ])
    .pipe(plumber())
    .pipe(sass.sync({ outputStyle: /*"expanded"*/ "compressed" }).on("error", sass.logError))
    // .pipe(cleanCSS())
    .pipe(rename({ suffix: ".min" }))
    .pipe(header(banner, { pkg: pkg }))
    .pipe(gulp.dest("./source/assets/css"))
    .pipe(gulp.dest("./_site/assets/css"))
    .pipe(browserSync.stream())
    .pipe(plumber.stop());
});

/*
 * Tarefa do Gulp para compilar arquivos JavaScript.
 * src: ./src/js
 * dest: ./source/assets/js, ./_site/assets/js
 */
gulp.task("js", function() {
  browserSync.notify(messages.js);
  return gulp
    .src(["./src/js/**/0*.js"])
    .pipe(plumber())
    .pipe(concat("main.js"))
    .pipe(uglify())
    .pipe(rename({ suffix: ".min" }))
    .pipe(header(banner, { pkg: pkg }))
    .pipe(gulp.dest("./source/assets/js"))
    .pipe(gulp.dest("./_site/assets/js"))
    .pipe(browserSync.stream())
    .pipe(plumber.stop());
});

/*
 * Tarefa do Gulp para minimizar arquivos de Imagem
 * src: ./src/img
 * dest: ./source/assets/img, ./_site/assets/img
 */
gulp.task("imagemin", function() {
  return gulp
    .src([
      "./src/img/**/*.{gif,jpg,jpeg,png,svg,ico}",
      "!./src/img/**/*.fw.png"
    ])
    .pipe(plumber())
    .pipe(
      imagemin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 5,
        svgoPlugins: [{ removeViewBox: false }]
      })
    )
    .pipe(gulp.dest("./source/assets/img"))
    .pipe(gulp.dest("./_site/assets/img"))
    .pipe(browserSync.stream())
    .pipe(plumber.stop());
});

// Copy third party libraries from /node_modules into /vendor
gulp.task("vendor", function() {
  // Font Awesome
  gulp
    .src([
      "./node_modules/font-awesome/**/*.min.*",
      "./node_modules/font-awesome/**/fonts/*",
      "!./node_modules/font-awesome/{less,less/*}",
      "!./node_modules/font-awesome/{scss,scss/*}",
      "!./node_modules/font-awesome/.*",
      "!./node_modules/font-awesome/*.{txt,json,md}",
      "!./node_modules/font-awesome/**/*.map"
    ])
    .pipe(gulp.dest("./source/assets/vendor/font-awesome"))
    .pipe(gulp.dest("./_site/assets/vendor/font-awesome"));

  // Zepto
  gulp
    .src([
      "./node_modules/zepto/dist/*.min.*",
      "!./node_modules/zepto/dist/*.map"
    ])
    .pipe(gulp.dest("./source/assets/vendor/zepto"))
    .pipe(gulp.dest("./_site/assets/vendor/zepto"));

  // Icons & Manifest
  // realfavicongenerator.net
  gulp
    .src(["./src/img/icons/**/*.{webmanifest,xml}"])
    .pipe(gulp.dest("./source/assets"))
    .pipe(gulp.dest("./_site/assets"));
});

/*
 * Tarefa do Gulp para limpar o diretório de saída
 * dir: ./site, ./assets, ./assets
 */
gulp.task("clean", function() {
  return del(["./_site", "./source/assets", "./src/svg-icons/icon"]);
});

/**
 * Watch stylus files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */
gulp.task("watch", function() {
   gulp.watch("./src/styl/**/*.styl", ["stylus"]);
 gulp.watch("./src/scss/**/*.scss", ["sass"]);
  gulp.watch("./src/js/**/*.js", ["js"]);
  gulp.watch(
    [
      "./source/*.{html,md,json}",
      "./source/admin/*.{html,yml}",
      "./source/pages/*.{html,md,json,yml}",

      "./source/_posts/*.{html,md}",
      "./source/_projects/*.{html,md}",
      "./source/_links/*.{html,md}",
      "./source/_data/*.{yml,json}",

      "./source/_layouts/*.{html,md}",
      "./source/_includes/*.{html,md}",

      "./_config.yml"
    ],
    ["jekyll-rebuild"]
  );
});

// Gulp task to minify all files
gulp.task("default", ["clean"], function() {
  runSequence("stylus", "sass", "js", "svg-icon", "imagemin", "vendor", "browser-sync", "watch");
});

// Gulp task to build all files
gulp.task("build", ["clean"], function() {
  runSequence("stylus", "sass", "js", "svg-icon", "imagemin", "vendor", "jekyll-build");
});
