---
title: Bem-vindo ao Jekyll
date: 2015-08-04 03:21:35 Z
categories:
- source
- Jekyll is awesome
tags:
- jekyll
- web
layout: post
author: Rodrigo Ribeiro
comments: false
description: Put your description here.
---

Você encontrará este post no diretório `source/_posts` - edite este post e reconstrua (ou rode com o switch` -w`) para ver suas mudanças!
Para adicionar novas mensagens, basta adicionar um arquivo no diretório `source/_posts` que segue a convenção: AAAA-MM-DD-nome-de-postagem.ext.

Jekyll também oferece suporte poderoso para trechos de código:


```ruby
def print_hi(name)
  puts "Oi, #{name}"
end
print_hi('Rodrigo')
#=> prints 'Oi, Rodrigo' to STDOUT.
```

Confira a [Jekyll docs][jekyll] para mais informações sobre como tirar o máximo proveito do Jekyll. Arquive todos os pedidos de bugs / recursos em [Jekyll's GitHub repo][jekyll-gh].

[jekyll-gh]: https://github.com/mojombo/jekyll
[jekyll]:    http://jekyllrb.com
